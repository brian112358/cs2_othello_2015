#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include "math.h"
#include "common.h"
#include "board.h"
using namespace std;

#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
	Board *board; // Board used to store Othello game state
    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
	vector<Move*> getMoves(Board *board, Side side);
	float scoreMove(Board *board, Move *move, Side side);
	float minimax(Board *board, int depth, float alpha, float beta, Side side);
	Move *monteCarlo(int n);
	Side side;	// Stores the side (BLACK/WHITE) the player is on
};

#endif
