Othello project for Caltech CS2, by Brian Lee.

Uses a Monte-Carlo Tree Search AI with additional heuristics for mobility and board positioning.

Improvements:
- Uses Monte-Carlo Tree Search AI instead of minimax
    + Allows the AI to plan moves farther ahead (especially useful during the endgame)
    + Uses less memory (<200k vs >200M for minimax)
- Added better heuristics which favor moves that:
    + Give the AI more moves than its opponent
    + Adds bonuses for certain positions (corners, middle edges, center 4x4 square)
    + Subtracts penalties for unfavorable positions (tiles adjacent to corners)

The problem with the Minimax algorithm for Othello is that it is difficult to evaluate intermediate game states, since the more pieces one player has, the easier it is for the other player to retake pieces. While improving the heuristics can increase the effectiveness of a Minimax AI, the Monte-Carlo tree search algorithm uses much less memory and computational power. It takes random valid moves, and evaluates the game board at the game's conclusion. This is a more accurate measure of the potential of a move. Heuristics were added to weigh moves that are better short-term, since the MCTS only examines long-term effects of a move.

I believe my AI will perform well during the tournament because a majority of people are using minimax algorithms. The MCTS algorithm can look farther ahead than any minimax algorithm, which can let it plan out its endgame (last 5 moves) very effectively. In tests against BetterPlayer and my own Minimax algorithm, the MCTS frequently goes from losing by 30+ pieces to winning by 
40+ pieces in the last 5 moves. It can even win games with only 0/1 corners.