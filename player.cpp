/**
 * @file player.cpp
 * @author Brian Lee <bllee@caltech.edu>
 *
 * @brief Defines the Player class, which plays a game of Othello.
 */

#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    board = new Board();
    this->side = side;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}


/**
 * Computes a vector with all the valid moves that the player can make at the
 * current game state.
 */
vector<Move*> Player::getMoves(Board *board, Side side) {
    vector<Move*> moves;
    for(int r = 0; r < 8; r++) {
        for(int c = 0; c < 8; c++) {
            Move *m = new Move(r, c);
            if(board->checkMove(m, side)) {
                moves.push_back(m);
            }
            else delete m;
        }
    }
    return moves;
}


const int boardScore[64] =
{
    20, -1, 3, 1, 1, 3, -1, 20,
    -1, -1, 0, 0, 0, 0, -1, -1,
     3,  0, 1, 1, 1, 1,  0,  3,
     1,  0, 1, 1, 1, 1,  0,  1,
     1,  0, 1, 1, 1, 1,  0,  1,
     3,  0, 1, 1, 1, 1,  0,  3,
    -1, -1, 0, 0, 0, 0, -1, -1,
    20, -1, 3, 1, 1, 3, -1, 20
};

/**
 * Scores the given move. Assumes that the given move is valid.
 * Bonuses for: each corner (+100%), each border (+10%)
 * Penalties for: tiles next to corners (-100%)
 */
float Player::scoreMove(Board *board, Move *move, Side side) {
    Side other = side==BLACK? WHITE:BLACK;

    //float bonus = 0;

    Board *newBoard = board->copy();
    if(move != NULL) newBoard->doMove(move, side);

    int score = 0;

    for(int x = 0; x < 8; x++) {
        for(int y = 0; y < 8; y++) {
            if(newBoard->get(side, x, y))
                score += boardScore[8*x + y];
            if(newBoard->get(other, x, y))
                score -= boardScore[8*x + y];
        }
    }

    // Bonus for minimizing opponent's moves
    vector<Move*> opponentMoves = getMoves(newBoard, other);
    //bonus += 0.2 * (4.0 - opponentMoves.size());

    score -= opponentMoves.size();
    //score += bonus * fabs(score);
    delete newBoard;
    for(unsigned int i = 0; i < opponentMoves.size(); i++)
        delete opponentMoves[i];
    return score;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {

    board->doMove(opponentsMove, (side==BLACK)? WHITE:BLACK);

    Move *m = NULL;

    if(testingMinimax) {
        vector<Move*> moves = getMoves(board, side);
        if(moves.size() == 0) return NULL;
        float maxScore = -10000;
        for(unsigned int i = 0; i < moves.size(); i++)
        {
            Board *newBoard = board->copy();
            newBoard->doMove(moves[i], side);
            float score = -minimax(newBoard, 3, -10000, 10000, side==BLACK? WHITE:BLACK);
            if(score > maxScore) {
                m = moves[i];
                maxScore = score;
            }
            delete newBoard;
        }
        for(unsigned int i = 0; i < moves.size(); i++)
            if(moves[i] != m) delete moves[i];
    }
    else {
        if(board->countTotal() > 0)
            return monteCarlo(20000);
        vector<Move*> moves = getMoves(board, side);
        if(moves.size() == 0) return NULL;
        float maxScore = -10000;
        for(unsigned int i = 0; i < moves.size(); i++)
        {
            Board *newBoard = board->copy();
            newBoard->doMove(moves[i], side);
            float score = -minimax(newBoard, 3, -10000, 10000, side==BLACK? WHITE:BLACK);
            if(score > maxScore) {
                m = moves[i];
                maxScore = score;
            }
            delete newBoard;
        }
        fprintf(stderr, "Minimax Move: (%d,%d), score: %f\n", m->getX(), m->getY(), maxScore);

        for(unsigned int i = 0; i < moves.size(); i++)
            if(moves[i] != m) delete moves[i];
    }

    board->doMove(m, side);    return m;
}

/**
 * Calculates the score for a given side with the minimax algorithm.
 * @param  board The board state to calculate with
 * @param  depth The number of levels to look down the decision tree
 * @param  side  The side to calculate the move for
 * @return       
 */
float Player::minimax(Board *board, int depth, float alpha, float beta, Side side) {
    vector<Move*> moves = getMoves(board, side);
    Side other = side==BLACK? WHITE:BLACK;
    vector<Move*> opponentMoves = getMoves(board, other);
    if( (moves.size() == 0 && opponentMoves.size()==0) || depth <= 0) {
        float score = scoreMove(board, NULL, side);
        for(unsigned int i = 0; i < opponentMoves.size(); i++)
            delete opponentMoves[i];
        return score;
    }

    for(unsigned int i = 0; i < opponentMoves.size(); i++)
            delete opponentMoves[i];

    if(moves.size() == 0) moves.push_back(NULL);

    float max = -10000;
    for(unsigned int i = 0; i < moves.size(); i++) {
        Board *newBoard = board->copy();
        newBoard->doMove(moves[i], side);
        float score = -minimax(newBoard, depth-1, -beta, -alpha, other);
        delete newBoard;
        max = MAX(max, score);
        alpha = MAX(alpha, max);
        if(alpha >= beta) break;
    }
    return max;
}

const int mcBoardScore[64] =
{
     30,-15, 5, 2, 2, 5,-15, 30
    -15,-15, 0, 0, 0, 0,-15,-15,
      5,  0, 1, 1, 1, 1,  0,  5,
      2,  0, 1, 1, 1, 1,  0,  2,
      2,  0, 1, 1, 1, 1,  0,  2,
      5,  0, 1, 1, 1, 1,  0,  5,
    -15,-15, 0, 0, 0, 0,-15,-15,
     30,-15, 5, 2, 2, 5,-15, 30
};

/**
 * @brief This AI does n Monte Carlo simulations on the current board (with
 * random moves at each turn) and picks the move with the best expected value +
 * bonus (given by mcBoardScore)
 */
Move *Player::monteCarlo(int n) {
    srand(time(NULL));
    Side other = this->side==BLACK? WHITE:BLACK;
    vector<Move*> moves = getMoves(board, side);
    if(moves.size() == 0) return NULL;
    int *totalScore = new int[moves.size()];
    int *numVisits = new int[moves.size()];
    for(unsigned int i = 0; i < moves.size(); i++) {
        totalScore[i] = 0;
        numVisits[i] = 0;
    }
    
    for(int j = 0; j < n; j++) {
        Side side = this->side;
        int i = rand() % moves.size();  // Choose random first move to simulate
        Board *newBoard = board->copy();
        newBoard->doMove(moves[i], side);

        while(!newBoard->isDone()) {
            side = side==BLACK? WHITE:BLACK;
            vector<Move*> nextMoves = getMoves(newBoard, side);
            Move *randomMove = NULL;
            totalScore[i] += 3/(newBoard->countTotal()) * 
                (side==this->side? 1:-1) * nextMoves.size();
            if(nextMoves.size() != 0) {
                randomMove = nextMoves[rand() % nextMoves.size()];
            }
            newBoard->doMove(randomMove, side);
            for(unsigned int i = 0; i < nextMoves.size(); i++)
                delete nextMoves[i];
        }
        totalScore[i] += ( newBoard->count(this->side)
            - newBoard->count(other) );
        totalScore[i] += ( newBoard->count(this->side) >
            newBoard->count(other))? 20:-20;
        numVisits[i]++;
        delete newBoard;
    }

    for(unsigned int i = 0; i < moves.size(); i++)
        totalScore[i] += (1.0 - board->countTotal()/128.0)*mcBoardScore[8*moves[i]->getX() + moves[i]->getY()];

    Move *m = NULL;
    float maxScore = -100;
    for(unsigned int i = 0; i < moves.size(); i++) {
        if(numVisits[i] != 0 && (float)totalScore[i]/(float)numVisits[i] + sqrt(2*log(n)/numVisits[i]) > maxScore) {
            m = moves[i];
            maxScore = (float)totalScore[i]/(float)numVisits[i];
        }
    }
    
    fprintf(stderr, "Monte Carlo Move: (%d,%d), score: %f\n", m->getX(), m->getY(), maxScore);

    for(unsigned int i = 0; i < moves.size(); i++)
        if(moves[i] != m) delete moves[i];

    delete[] totalScore;
    delete[] numVisits;

    this->board->doMove(m, this->side);
    return m;
}